Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })

beforeEach(()=> {
    cy.visit('https://staging.pigspin.xyz')
    cy.viewport(1024,768)
})

describe('Smoke Test - Staging',() => {
    var user_pin = ['1','1','1','1','1','1']
    var user_pin_test = ['2','2','2','2','2','2']

    it('TC_0001 Register sucess ',() => {
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0300001100')
        cy.get('[id="submit-checkphone-button"]').click()

        //VERIFY OTP
        cy.contains('สมัครสมาชิก').should('be.visible')
        cy.get('[id="otp-refcode"]').invoke('text')
            .then((text)=>{ 
                var fullText = text;
                var pattern = /[0-9]{6}/g;
                const number = fullText.match(pattern);
                cy.get('[id="otp-input"]').type(number[0])
           }
        )
        //VERIFY KYC
        cy.wait(1000)
        cy.get('[id="citizen-id-input"]').type('1141000070309')
        cy.get('[id="laser-number-input"]').type('JT3103599958')
        cy.get('[id="name-input"]').type('ธรรมนูญ อิ่มศรี')
        cy.get('[id="date-input-inactive"]').click()
        cy.get('[id="date-input-day"]').type('21')
        cy.get('[id="date-input-month"]').type('10')
        cy.get('[id="date-input-year"]').type('2541')
        cy.get('[id="userinfo-submit-button"]').click()
        
        cy.contains('กรุณารอสักครู่').should('be.visible')
        
        //VERIFY BANK
        cy.wait(1000)
        cy.contains('เลือกบัญชีธนาคาร').should('be.visible')
        // id="bankaccount-name-input" 
        cy.get('[id="bankitem-bank-ksb"]').click()
        cy.get('[id="bankaccount-number-input"]').type('0928071049')
        cy.get('[id="bankselect-submit-button"]').click()

        cy.contains('กรุณารอสักครู่').should('be.visible')

        //VERIFY Summary
        cy.wait(1000)
        cy.contains('ข้อมูลการลงทะเบียน').should('be.visible')
        
        
    })
    // it('TC_0004 Login and Play demo game',() =>{
    //     cy.contains("เกมยอดนิยม").scrollIntoView()
    // })

    it('TC_0007 Login and Generate QR success ',() => {
        
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0874809618')
        cy.get('[id="submit-checkphone-button"]').click()

        //LOGIN PIN
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
        //CHECK QR genearate
        cy.get('[class="flex items-center justify-end ml-1"]').click()
        cy.contains("เติมเครดิต").click()
        cy.get('img[src="/default/assets/images/payment-option/bank-qr2.png"]').click()
        cy.get('input').type('100')
        cy.contains("สร้าง QR").click()
        cy.contains('ชื่อบัญชี').should('be.visible')
        cy.contains('จำนวนเงิน').should('be.visible')
    })

    it('TC_0008 Login and Get user info',() => {
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0874809618')
        cy.get('[id="submit-checkphone-button"]').click()

        //LOGIN PIN
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
        //CHECK USER INFO
        cy.get('[data-icon="bars"]').click()
        cy.contains("ข้อมูลส่วนตัว").click()
        cy.contains('ชื่อ-นามสกุล (ไทย)').should('be.visible')
        cy.contains('เบอร์โทรศัพท์มือถือ').should('be.visible')
        cy.contains('หมายเลขบัตรประชาชน').should('be.visible')
        cy.contains('หมายเลขหลังบัตรประชาชน').should('be.visible')
        cy.contains('วัน เดือน ปีเกิด').should('be.visible')
    })

    it('TC_0009 Login and Change PIN',() => {
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0874809618')
        cy.get('[id="submit-checkphone-button"]').click()

        var user_pin = ['1','1','1','1','1','1']
        //LOGIN PIN
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })

        //CHECK CHANGE PIN
        cy.get('[data-icon="bars"]').click()
        cy.contains("เปลี่ยน PIN").click()
        cy.contains("ยืนยันการเปลี่ยน PIN").should('be.visible')
        cy.contains('button', 'ยืนยัน').click()

         //OLD PIN
        cy.contains('ใส่ PIN เดิม').should('be.visible')
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })

        //NEW PIN
        cy.wait(1000)
        cy.contains('ตั้ง PIN ใหม่ 6 หลัก').should('be.visible')
        cy.wrap(user_pin_test).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
   
        //CONFIRM NEW PIN
        cy.wait(1000)
        cy.contains('ยืนยัน PIN ใหม่อีกครั้ง').should('be.visible')
        cy.wrap(user_pin_test).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })

        cy.wait(1000)
        cy.contains('ระบบกำลังดำเนินการ').should('be.visible')

        cy.wait(1000)
        cy.contains('เปลี่ยน PIN สำเร็จ').should('be.visible')
        cy.contains("ตกลง").click()

        //RESETPIN TO NEW CASE 
        cy.contains("เปลี่ยน PIN").click()
        cy.contains("ยืนยันการเปลี่ยน PIN").should('be.visible')
        cy.contains('button', 'ยืนยัน').click()

         //OLD PIN
        cy.contains('ใส่ PIN เดิม').should('be.visible')
        cy.wrap(user_pin_test).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
        
        //NEW PIN
        cy.wait(1000)
        cy.contains('ตั้ง PIN ใหม่ 6 หลัก').should('be.visible')
        var user_pin = ['1','1','1','1','1','1']
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
        
        //CONFIRM NEW PIN
        cy.wait(1000)
        cy.contains('ยืนยัน PIN ใหม่อีกครั้ง').should('be.visible')
        var user_pin = ['1','1','1','1','1','1']
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })

        cy.wait(1000)
        cy.contains('ระบบกำลังดำเนินการ').should('be.visible')

        cy.wait(1000)
        cy.contains('เปลี่ยน PIN สำเร็จ').should('be.visible')
        cy.contains("ตกลง").click()
    })
    it('TC_00010 Login and ResetPIN sucess',() => {
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0874809618')

        //LOGIN
        cy.get('[id="submit-checkphone-button"]').click()
        cy.get('[id="forgot-pin-button"]').click()
        
        //VERIFIED IDENTITY
        cy.contains('ดำเนินการยืนยันตัวตน และตั้งรหัสผ่านใหม่').should('be.visible')
        cy.get('[id="citizen-id-input"]').type('1100600357691')
        cy.get('[id="reset-pin-phone-number"]').type('0874809618')
        cy.contains('button', 'ถัดไป').click()

        //VERIFY OTP
        cy.get('[id="otp-refcode"]').invoke('text')
            .then((text)=>{ 
                var fullText = text;
                var pattern = /[0-9]{6}/g;
                const number = fullText.match(pattern);
                cy.get('[id="otp-input"]').type(number[0])
           }
        )
        cy.wait(1000)
        cy.contains('ตั้ง PIN รหัสผ่าน 6 หลัก').should('be.visible')
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
        cy.wait(1000)
        cy.contains('ยืนยันรหัสผ่านอีกครั้ง').should('be.visible')
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
    })


})