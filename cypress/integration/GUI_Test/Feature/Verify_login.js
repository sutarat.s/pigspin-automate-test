Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })

beforeEach(()=> {
    cy.visit('https://staging.pigspin.xyz')
})

describe('Verify login',() => {
    
    it('TC_0001 Success login',() => {
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0874809618')
        cy.get('[id="submit-checkphone-button"]').click()
        //LOGIN PIN
        var user_pin = ['1','1','1','1','1','1']
        cy.wrap(user_pin).each((ele)=>{
            cy.get('[id="numpad-'+ele+'"]').click()
        })
        cy.get('[alt="balance"]').should('be.visible')
    })

    it('TC_0002 Unsuccess login with inactive account',() => {
        cy.contains("สมัครสมาชิก").click()
        cy.get('input').type('0920050079')
        cy.get('[id="submit-checkphone-button"]').click()
        cy.contains("พบปัญหาการเข้าใช้งาน").should('be.visible')
        cy.get('button').should('have.value','ลองใหม่อีกครั้ง')
        cy.get('button').should('have.value','LINE @PIGSPIN')    
    })

    // it('TC_0003 Unsuccess login with blocked PIN account',() => {
        
    
})